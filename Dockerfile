FROM golang:1.14 as builder
WORKDIR /User/container/mqtt-build
ARG goos=linux
ARG goarch=amd64
ARG tag=latest
RUN go mod init mqtt-build &&\
    go get gitlab.com/aahme144/mqtt-broker@$tag
ENV GOOS=$goos \
    GOARCH=$goarch \
    CGO_ENABLED=0
RUN cd  "$(\ls -1dt $GOPATH/pkg/mod/gitlab.com/aahme144//*/ | head -n 1)" &&\
    go build -o /User/container/mqtt-build/mqtt-broker -a -ldflags '-extldflags "-static"' .


FROM registry:2.7.1
WORKDIR /
COPY --from=builder /User/container/mqtt-build/mqtt-broker /bin/mqtt-broker
RUN chmod +x /bin/mqtt-broker
EXPOSE 1883

ENTRYPOINT ["mqtt-broker"]
CMD ["-p", "1883","-router",":9888","--clusterport","1993"]