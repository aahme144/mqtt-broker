High Performance MQTT Broker
============

## About
Golang MQTT Broker, Version 3.1.1, and Compatible with mosquitto-client

## K8s Cluster support

### Requirements
- Fully fledged kubernetes setup

This MQTT Broker (Cluster) eliminates the possibility of a single point of failure which is the bottle neck of having one stateless borker

Steps to run the broker cluster on k8s
- Start the mqtt k8s router
- kubectl apply -f ./k8s/configmap/mqtt-broker-config.yml
- kubectl apply -f ./k8s/configmap/mqtt-broker-config.yml

Cluster can now be reached at any of the k8s node ip at port 30001