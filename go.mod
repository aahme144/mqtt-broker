module gitlab.com/aahme144/mqtt-broker

go 1.12

require (
	github.com/Shopify/sarama v1.27.0
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.2
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/segmentio/fasthash v1.0.3
	github.com/stretchr/testify v1.6.0
	github.com/tidwall/gjson v1.6.1
	go.uber.org/zap v1.16.0
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
)
